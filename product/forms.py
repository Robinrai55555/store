from django.contrib.auth.models import User
from django import forms
from .models import*


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ['title', 'image']
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control mb-3',
                'placeholder': 'enter name'
            }),
            'images': forms.ClearableFileInput(attrs={
                'class': 'form-control mb-3',
            }), }


class Sub_CategoryCreateForm(forms.ModelForm):
    class Meta:
        model = Sub_Category
        fields = ['title', 'category']
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control mb-3',
                'placeholder': 'enter name'
            }),
            'category': forms.TextInput(attrs={
                'class': 'form-control mb-3',
                'placeholder': 'enter category'
            }), }


class QuentityCreateForm(forms.ModelForm):
    class Meta:
        model = Quentity
        fields = ['title']
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control mb-3',
                'placeholder': 'enter name'
            }),
        }


class ProductCreateForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['name', 'images', 'price', 'category', 'weight', 'quentity']
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control mb-3',
                'placeholder': 'enter name'
            }),
            'images': forms.ClearableFileInput(attrs={
                'class': 'form-control mb-3',
            }),
            'price': forms.NumberInput(attrs={
                'class': 'form-control mb-3',
                'placeholder': 'enter the price'
            }),
            'category': forms.Select(attrs={
                'class': 'form-control mb-3',
                'placeholder': 'enter category'
            }),
            'weight': forms.NumberInput(attrs={
                'class': 'form-control mb-3',
                'placeholder': 'enter weight'
            }),
            'quentity': forms.NumberInput(attrs={
                'class': 'form-control mb-3',
                'placeholder': 'enter quentity'
            }),
        }
        
class LoginForm(forms.Form):
    username=forms.CharField(widget=forms.TextInput())
    password=forms.CharField(widget=forms.PasswordInput())
