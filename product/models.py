from django.db import models

# Create your models here.


class Category(models.Model):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to="category")

    def __str__(self):
        return self.title


class Sub_Category(models.Model):
    title = models.CharField(max_length=200)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    

    def __str__(self):
        return self.title


class Quentity(models.Model):
    title = models.CharField(max_length=200)

    def __str__(self):
        return self.title


class Product(models.Model):
    name = models.CharField(max_length=200)
    images = models.ImageField(upload_to='product', null=True, blank=True)
    price = models.DecimalField(decimal_places=3, max_digits=19)
    category = models.ForeignKey(Sub_Category, on_delete=models.CASCADE)
    weight = models.DecimalField(decimal_places=3, max_digits=19)
    quentity = models.ForeignKey(Quentity, on_delete=models.CASCADE)
    #sub_category = models.ForeignKey(Sub_Category, on_delete=models.CASCADE)
    view_count = models.PositiveIntegerField(default=0)
    def __str__(self):
        return self.name
