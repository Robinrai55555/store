from django.urls import path
from .views import*

app_name = "product"

urlpatterns = [
    path('', Home.as_view(), name='home'),
    path('about/', AboutView.as_view(), name='about'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('product/', ProductView.as_view(), name='product'),
    path('category/<int:pk>/',
         CategoryDetailView.as_view(), name='categorydetail'),
    path('kitchen/', KitchenView.as_view(), name="kitchen"),
    path('product/<int:pk>/details/',
         ProductDetailsView.as_view(), name="details"),
    path('product/create/', ProductCreateView.as_view(), name="create"),
    path('search/', SearchView.as_view(), name="search"),
    path('restric/', RestricView.as_view(), name="restric"),
    path('firm-admin/', FormAdminView.as_view(), name="adminhome"),
    path('firm-admin/product/list/',
         AdminProductListView.as_view(), name="adminproductlist"),
    path('firm-admin/product/add/',
         AdminProductAddView.as_view(), name="adminproductadd"),
    path('firm-admin/product/<int:pk>/create/',
         AdminProductUpdateView.as_view(), name="adminproductupdate"),
    path('firm-admin/product/<int:pk>/delete/',
         AdminProductDeleteView.as_view(), name="adminproductdelete"),
    path('firm-admin/category/list/',
         AdminCategoryListView.as_view(), name="admincategorylist"),
    path('firm-admin/category/add/',
         AdminCategoryAddView.as_view(), name="admincategoryadd"),
    path('firm-admin/category/<int:pk>/update/',
         AdminCategoryUpdateView.as_view(), name="admincategoryupdate"),
]
    
