from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from django.views.generic import*
from .models import*
from .forms import*
from django.db.models import Q


class BaseMixin(object):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categorylist'] = Category.objects.all()
        return context


class Home(BaseMixin, TemplateView):
    template_name = "home.html"


class AboutView(BaseMixin, TemplateView):
    template_name = "about.html"


class LoginView(BaseMixin, TemplateView):
    template_name = "login.html"
# Create your views here.


class ProductView(BaseMixin, TemplateView):
    template_name = "product.html"


class ProductCreateView(CreateView):
    template_name = "create.html"
    form_class = ProductCreateForm
    success_url = "/"


class CategoryDetailView(BaseMixin, DetailView):
    template_name = "households.html"
    model = Category
    context_object_name = "categoryobject"


class KitchenView(TemplateView):
    template_name = "kitchen.html"


class ProductDetailsView(DetailView):
    template_name = "vegitable.html"
    # template_name = "admintemplates/adminbase.html"
    model = Product
    context_object_name = "product_detail"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        post = self.object
        post.view_count += 1
        post.save()
        return context


class SearchView(TemplateView):
    template_name = "search.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # query = self.request.GET["keyi"]
        query = self.request.GET.get('keyi')
        results = Product.objects.filter(
            Q(name__icontains=query) | Q(category__title__icontains=query))

        context["search_results"] = results
        return context
    # def post(self, request):
    #     query = request.POST.get('keyi')
    #     results = Product.objects.filter(
    #         Q(name__icontains=query) | Q(category__title__icontains=query))
    #     return render(request, self.template_name, {'search_results': results})


class SingleView(TemplateView):
    template_name = "single.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['port'] = Product.objects.all(Q())
        return context


class RestricView(TemplateView):
    template_name = "create.html"

    def dispatch(self, request, *args, **kwargs):
        usr = request.user
        if usr.is_authenticated:
            pass
        else:
            return redirect('/login/')
        return super().dispatch(request, *args, **kwargs)


class FormAdminView(TemplateView):
    template_name = 'admintemplates/adminhome.html'


class AdminProductListView(ListView):
    template_name = "admintemplates/productlist.html"
    queryset = Product.objects.all()
    context_object_name = "productlist"


class AdminProductAddView(CreateView):
    template_name = "admintemplates/productcreate.html"
    form_class = ProductCreateForm
    success_url = '/firm-admin/product/list/'


class AdminProductUpdateView(UpdateView):
    template_name = "admintemplates/adminupdate.html"
    form_class = ProductCreateForm
    model = Product
    success_url = '/firm-admin/product/list/'


class AdminProductDeleteView(DeleteView):
    template_name = "admintemplates/adminproductdelete.html"
    model = Product
    success_url = '/firm-admin/product/list/'


class AdminCategoryListView(ListView):
    template_name = "admintemplates/admincategorylist.html"
    queryset = Category.objects.all()
    context_object_name = "categoryobjectt"


class AdminCategoryAddView(CreateView):
    template_name = "admintemplates/admincategoryadd.html"
    form_class = CategoryForm
    success_url = "/firm-admin/category/list/"


class AdminCategoryUpdateView(UpdateView):
    template_name = "admintemplates/admincategoryupdate.html"
    form_class = CategoryForm
    model = Category
    success_url = "/firm-admin/category/list/"


class LoginView(FormView):
    template_name = "login.html"
    form_class = LoginForm
    success_url = "/firm-admin/"

    def form_valid(self, form):
        a = form.cleaned_data['username']
        b = form.cleaned_data.get('password')
        usr = authenticate(username=a, password=b)
        if usr is not None:
            login(self.request, usr)
        else:
            return render(self.request, self.template_name,
                          {"error": "invalid creditentials", 'form': form})

        return super().form_valid(form)


class LogoutView(View):
    def get(self,request):
        logout(request)
        return redirect("/")